//META{"name":"DblClickDelete"}*//


/**
 *  @version es6
 *  @author BART96
 *  @name DblClickDelete
**/

class DblClickDelete {
  getName         () {return "Double click delete";}
  getDescription  () {return "Double click messages to delete them";}
  getVersion      () {return "0.0.1";}
  getAuthor       () {return "Jiiks & BART96";}

  onMessage       () {};
  onSwitch        () {};

  load            () {this.start;}
  unload          () {this.stop;}

  stop            () {$(document).off("dblclick.dce");}
  start           () {
    $(document).on("dblclick.dce", function(e) {
        var target = $(e.target);
        if (target.parents(".message").length > 0) {
            var msg = target.parents(".message").first();
            var opt = msg.find(".btn-option");
            opt.click();

            $.each($('.button-1ZXqCA'), (index, value) => {
                var option = $(value);
                if (option.text() === "Delete") {
                    option.click();
                }
            });

            $("[class='theme-dark'] *").css({"opacity":0, "z-index":0});

            if ($("[class^='flex-'] [type='submit'] div").text() === "Delete")
                $("[class^='flex-'] [type='submit'] div").click();
        }
    });
  };
}
